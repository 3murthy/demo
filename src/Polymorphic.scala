import scala.annotation.tailrec

object Polymorphic extends App {
  var arr = Array(10, 20, 40, 70, 50)
  var num = 1
  println(fun1(arr, num))

  def fun1(ar: Array[Int], x: Int): Int = {
    @tailrec
    def fun2(n: Int): Int =
      if (ar(n) == ar(x)) ar(n)
      else
        fun2(n + 1)

    fun2(0)

  }
}
