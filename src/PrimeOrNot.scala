object PrimeOrNot extends App {
  var num = 2
  var count = 0

  prime(num)
  def prime(n: Int): Unit = {
    if(n>0) {
      if (num % n == 0) {
        count += 1
      }
      prime(n - 1)
    }
  }
  if (count == 2) {
    println(num + " is prime number..")
  } else {
    println(num + " is not prime number..")
  }
}
