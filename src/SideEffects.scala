object SideEffects extends App {
  var a:Int=10;var b:Int=20
  println(WithSideEffects.sum(a, b))
  println(WithoutSideEffects.sum(20, 30))

}

object WithSideEffects {
  def sum(x: Int, y: Int): Unit = x + y
}

object WithoutSideEffects {
  def sum(x: Int, y: Int): Int = x + y
}
