import scala.annotation.tailrec

object Monomorphic extends App {
  var arr = Array("hi", "hello", "Welcome", "to", "scala")
  var num = 0
  println(arFun(arr, num))

  def arFun(ar: Array[String], str: Int): String = {
    @tailrec
    def inFun(n: Int): String =
      if (ar(n) == ar(str)) ar(n)
      else
        inFun(n + 1)

    inFun(0)
  }
}
